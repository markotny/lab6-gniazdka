package tb.sockets.client;

import java.io.*;
import java.net.Socket;

public class Konsola {

	public static void main(String[] args) {
		try {
			Socket sock = new Socket("127.0.0.1", 5555);

			BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));

			OutputStream outputStream = sock.getOutputStream();
			PrintWriter printWriter = new PrintWriter(outputStream, true);

			InputStream inputStream = sock.getInputStream();
			BufferedReader receiveRead = new BufferedReader(new InputStreamReader(inputStream));

		System.out.println("Connected to server.");

		String receiveMessage, sendMessage;
		while(true)
		{
			sendMessage = keyRead.readLine();
			printWriter.println(sendMessage);
			printWriter.flush();
			if((receiveMessage = receiveRead.readLine()) != null)
			{
				System.out.println(receiveMessage);
			}
		}
		}
		catch (Exception e){
			System.out.println(e.getMessage());
	}
}

}
