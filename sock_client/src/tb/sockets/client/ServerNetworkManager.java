package tb.sockets.client;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerNetworkManager extends NetworkManager{

    private int port = 8000;
    private ServerSocket serverSocket;
    private BufferedReader bufferedReader;

    public void startServer(int port){
        this.port = port;

        try {
            serverSocket = new ServerSocket(this.port);
            Socket sock = serverSocket.accept();

            OutputStream oStream = sock.getOutputStream();
            printWriter = new PrintWriter(oStream, true);

            inputStream = sock.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            connected = true;
        }
        catch (Exception e){
            System.out.println("Socket server problem / application closed, " + e.toString());
        }
    }

    public void shutdown(){
        try {
            send("ServerShutdown");
            Thread.sleep(10);
            serverSocket.close();
            connected = false;
        }
        catch (Exception e){
            System.out.println("Can't shut down socket server, " + e.toString());
        }
    }

    public String readMessage(){
        String receiveMessage;
        try {
            receiveMessage = bufferedReader.readLine();
            if(receiveMessage != null){
                connected = true;
                System.out.println(receiveMessage);
                if(receiveMessage.equals("@reboot")){
                    shutdown();
                    startServer(this.port);
                }
                if(receiveMessage.equals(""))
                    receiveMessage = "Client disconnected.";
            }
            else if(connected){
                connected = false;
                shutdown();
                startServer(this.port);
            }
        }
        catch (Exception e){
            System.out.println("Unable to read receive buffer, " + e.toString());
            return "";
        }

        return receiveMessage;
    }

}
