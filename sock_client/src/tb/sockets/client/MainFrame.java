package tb.sockets.client;

import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.net.Socket;

public class MainFrame extends JFrame implements Runnable{

	private JPanel contentPane;
    private JLabel lblNotConnected;
    NetworkManager networkManager;

    JTextField msgField;
    OrderPane panel;
    MainFrame frameRef;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
        frameRef = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHost = new JLabel("Host:");
        JLabel lblPort = new JLabel("Port:");
        JTextField frmtdtxtfldIp = new JTextField("127.0.0.1");
        JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
        JButton btnConnect = new JButton("Connect");
        lblNotConnected = new JLabel("Not Connected");
        JRadioButton btnClient = new JRadioButton("Client");
        JRadioButton btnServer = new JRadioButton("Server");
        msgField = new JTextField();
        JButton btnSend = new JButton("Send");


		lblHost.setBounds(10, 14, 50, 14);
		contentPane.add(lblHost);

        lblPort.setBounds(10, 42, 50, 14);
        contentPane.add(lblPort);


        frmtdtxtfldIp.setBounds(63, 11, 90, 20);
        contentPane.add(frmtdtxtfldIp);


        frmtdtxtfldXxxx.setText("6666");
        frmtdtxtfldXxxx.setBounds(63, 39, 90, 20);
        contentPane.add(frmtdtxtfldXxxx);

        try{
            btnConnect.addActionListener((ActionEvent)-> {
                if (networkManager == null || !networkManager.isConnected()) {
                    if (btnClient.isSelected()) {
                        networkManager = new ClientNetworkManager();
                        Connect(frmtdtxtfldIp.getText(), Integer.parseInt(frmtdtxtfldXxxx.getText()));
                        btnConnect.setText("Disconnect");
                    } else {
                        networkManager = new ServerNetworkManager();
                        startServer(Integer.parseInt(frmtdtxtfldXxxx.getText()));
                        btnConnect.setText("Stop server");
                    }

                    cancel = false;
                    (new Thread(frameRef)).start();
                    btnSend.setEnabled(true);
                }
                else{
                    networkManager.shutdown();
                    lblNotConnected.setText("Not Connected");
                    btnSend.setEnabled(false);
                    btnConnect.setText("Connect");
                    CancelReading();
                }
            });
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        btnConnect.setBounds(10, 70, 123, 23);
        contentPane.add(btnConnect);

        lblNotConnected.setForeground(new Color(255, 255, 255));
        lblNotConnected.setBackground(new Color(128, 128, 128));
        lblNotConnected.setOpaque(true);
        lblNotConnected.setBounds(10, 104, 123, 23);
        contentPane.add(lblNotConnected);

        btnClient.setBounds(10,130,100,20);
        btnClient.setSelected(true);
        btnClient.addActionListener((ActionEvent)->frmtdtxtfldIp.setEnabled(true));
        contentPane.add(btnClient);

        btnServer.setBounds(10,150,100,20);
        btnServer.addActionListener((ActionEvent)->frmtdtxtfldIp.setEnabled(false));
        contentPane.add(btnServer);

        ButtonGroup btnGroup = new ButtonGroup();
        btnGroup.add(btnClient);
        btnGroup.add(btnServer);

		panel = new OrderPane();
		panel.setBounds(165, 14, 487, 448);
		contentPane.add(panel);

		msgField.setBounds(165,465,487,30);
		msgField.addActionListener((ActionEvent)->{
            if(!msgField.getText().isEmpty() && networkManager.isConnected()){
                try {
                    sendMessage(msgField.getText());
                }
                catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        });
		contentPane.add(msgField);

		btnSend.setBounds(80,465,80,30);
		btnSend.setEnabled(false);
		btnSend.addActionListener((ActionEvent)->{
		    if(!msgField.getText().isEmpty() && networkManager.isConnected()){
		        try {
                    sendMessage(msgField.getText());
                }
                catch (Exception e){
		            System.out.println(e.getMessage());
                }
            }
        });
		contentPane.add(btnSend);



	}

	private void sendMessage(String msg){
	    try{
            System.out.println("Sending message:" + msg);
            networkManager.send(msg);
            panel.addSentMessage(msg);
            msgField.setText("");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

	private void Connect(String ip, int port){
        if(networkManager.connect(ip, port)){
            networkManager.send("connected");
            lblNotConnected.setText("connected");
        }
	}

    private void startServer(int port){
	    networkManager.startServer(port);
	    lblNotConnected.setText("Server Started");

    }

	public void receivedMsg(String msg){
	    panel.addReceivedMessage(msg);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        networkManager.shutdown();
    }

    volatile boolean cancel = false;
    private void CancelReading(){
        cancel = true;
    }
    @Override
    public void run() {
        while(!cancel){
            try{
                Thread.sleep(100);
                if (networkManager.isConnected() && networkManager.hasMessageToRead()){
                    String in = networkManager.readMessage();
                    SwingUtilities.invokeLater(() -> frameRef.receivedMsg(in));
                }
            }
            catch (Exception e){
                System.out.println("Error in receiving msg thread: " + e.getMessage());
            }
        }
    }
}
