package tb.sockets.server;

import javax.swing.*;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OrderPane extends JTextArea {

	Calendar cal;
	SimpleDateFormat sdf;
	/**
	 * Create the panel.
	 */
	public OrderPane() {
		super();
		setBackground(new Color(255, 255, 240));
		setEditable(false);
		setLineWrap(true);
		sdf = new SimpleDateFormat("HH:mm:ss");

	}
	public void addSentMessage(String text){
		cal = Calendar.getInstance();
		String msg = sdf.format(cal.getTime());
		msg = msg + ": >>" + text +"\n";
		this.append(msg);
	}
	public void addReceivedMessage(String text){
		cal = Calendar.getInstance();
		String msg = sdf.format(cal.getTime());
		msg = msg + ": <<" + text +"\n";
		this.append(msg);
	}

}
