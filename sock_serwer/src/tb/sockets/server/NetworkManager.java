package tb.sockets.server;

import java.io.InputStream;
import java.io.PrintWriter;

public abstract class NetworkManager {

    protected InputStream inputStream;
    protected PrintWriter printWriter;
    protected boolean connected = false;

    public void send(String input) {
        try {
            this.printWriter.println(input);
            this.printWriter.flush();
        }
        catch (Exception e) {
            System.out.println("failed to send msg to server, " + e.toString());
        }
    }
    public boolean connect(String ConnectIP, int port){return false;}
    public void startServer(int port){}
    public abstract String readMessage();
    public abstract void shutdown();

    public boolean isConnected(){
        return connected;
    }

    public boolean hasMessageToRead(){
        try {
            if (inputStream.available() > 0)
                return true;
            else
                return false;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return false;
    }
}
