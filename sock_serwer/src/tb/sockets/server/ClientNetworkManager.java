package tb.sockets.server;

import java.io.*;
import java.net.*;
import java.util.logging.Logger;

public class ClientNetworkManager extends NetworkManager{


    private BufferedReader receiveRead;
    private Socket sock;

    private int status = 0;

    public ClientNetworkManager() {
    }

    public boolean connect(String ConnectIP, int port) {
        try {
            sock = new Socket(ConnectIP, port);

            OutputStream outputStream = sock.getOutputStream();
            this.printWriter = new PrintWriter(outputStream, true);

            inputStream = sock.getInputStream();
            this.receiveRead = new BufferedReader(new InputStreamReader(inputStream));

            status = 1;
        }
        catch (Exception e) {
            System.out.println("Can't connect to server, " + e.toString());
            status = 0;
            return false;
        }

        return true;
    }

    public String readMessage() {
        try {
            String receiveMessage;
            if ((receiveMessage = this.receiveRead.readLine()) != null) {
                return receiveMessage;
            }
        }
        catch (Exception e) {
            System.out.println("receive() failed to read buffer, " + e.toString());
        }

        return "ServerError";
    }



    public void shutdown(){
        try {
            if (sock != null){
                send("");
                sock.close();
                status = 0;
            }
        }
        catch (Exception e){
            System.out.println("unable to disconnect/no active connection, " + e.toString());
        }
    }

    public int getStatus() {
        return status;
    }

    public boolean isConnected(){
        if (status==1)
            return true;
        else
            return false;
    }
}

